import csv
from bs4 import BeautifulSoup

# Název vstupního HTML souboru
input_file = 'PYdat\IntGPS.txt'

# Název výstupního CSV souboru
output_file = 'PYdat\IntGPS.csv'

# Načtení HTML souboru s explicitním určením kódování UTF-8
with open(input_file, mode='r', encoding='utf-8') as file:
    html_content = file.read()

# Inicializace BeautifulSoup pro zpracování HTML
soup = BeautifulSoup(html_content, 'html.parser')

# Nalezení všech řádků v tabulce
rows = soup.find_all('tr')

# Seznam pro ukládání dat
data = []

# Procházení všech řádků kromě prvního (obsahuje záhlaví)
for row in rows[1:]:
    # Nalezení všech buněk v řádku
    cells = row.find_all('td')
    
    # Extrahování hodnot z buněk a přidání do seznamu dat
    values = [cell.text.strip() for cell in cells]
    data.append(values)

# Otevření výstupního souboru CSV a zápis dat s explicitním určením kódování UTF-8
with open(output_file, mode='w', newline='', encoding='utf-8') as file:
    writer = csv.writer(file, delimiter=';')
    
    # Zápis dat do souboru CSV
    for row in data:
        writer.writerow(row)
