import pandas as pd
from pyproj import Proj, transform

# Definování projekcí
s_jtsk = Proj(init='epsg:5514')
wgs84 = Proj(init='epsg:4326')

input_file = 'souradnice\IntGPSF.csv'
output_file = 'souradnice\IntGPS_final.csv'

data = pd.read_csv(input_file)

# Funkce pro převod souřadnic
def convert_coords(x, y):
    lon, lat = transform(s_jtsk, wgs84, y, x)
    return lon, lat

# Převod souřadnic v každém řádku
counter = 0
total_rows = len(data)

def process_row(row):
    global counter
    lon, lat = convert_coords(row['d'], row['e'])
    counter += 1
    print(f"Processed {counter}/{total_rows} rows")
    return pd.Series([lon, lat])

data[['d', 'e']] = data.apply(process_row, axis=1)

# Uložení výsledků do nového souboru
data.to_csv(output_file, index=False)

print(f"Conversion completed. Total rows processed: {counter}")
